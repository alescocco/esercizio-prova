## Color Game

- Verranno create 4 squadre. Ognuna sarà associata ad un diverso branch che lavorerà su una propria cartella del progetto: red, yellow, green, blue 
- Ogni cartella conterrà un file list.md e una cartella /img 
- Ogni membro di una squadra dovrà inserire nella lista almeno 3 oggetti ognuno dei quali deve comunemente far riferimento al colore corrispondente (es. colore: bianco => oggetto latte ) e per ogni oggetto dovrà aggiungere un file immagine con lo stesso nome nella cartella /img,  che lo rappresenti 
- Ogni oggetto inserito dovrà essere riportato nel repository corredato della sua immagine, con un singolo commit ed un messaggio chiaro 
- Ogni oggetto inserito correttamente darà alla squadra 1 punto; ogni errore toglierà alla squadra 2 punti 
- La lista di ogni squadra ad inizio gioco avrà un elemento iniziale. **Verranno assegnati 7 punti bonus alle squadre che riusciranno a rimuovere con un solo commit l'elemento iniziale della propria lista e la relativa immagine**
- Vincerà la squadra con più punti alle 18 del 21/7