# Corso Git 

## Argomenti trattati: 

1. Concetti base 
1. Usare i branches 
1. Gestire conflitti 

## Esercizi svolti: 

1. Branch e Commit
1. Merge
1. Remote e push
1. Clone


